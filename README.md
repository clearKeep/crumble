# crumble

<p align="center">
  <a href="https://codeberg.org/clearKeep/crumble/tree/trunk"><img src="https://img.shields.io/badge/Branch-Trunk-%23eeeecd" alt="Trunk"/></a>
  <a href="https://codeberg.org/clearKeep/crumble/tree/drehmari"><img src="https://img.shields.io/badge/Branch-Drehmari-%23f06668.svg" alt="Drehmal Compat"/></a>
  <a href="https://codeberg.org/clearKeep/crumble/tree/mod-compat"><img src="https://img.shields.io/badge/Branch-Compat-ffff73" alt="Mod Compat"/></a>
</p>

<p align="center">
  <a href="https://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" src="cc-by-sa.png"/></a>
</p>

crumble is a work-in-progress resource pack for MC Java Edition.


# Contributing

Pretty much anything would help! Textures, models, sounds, fancy model/sound hacks or just issue reporting. Please remember that any contributions must be under a free license to comply with Codeberg's TOS. Some good resources to get started:

[Resource Pack Guide](https://github.com/Love-And-Tolerance/resource-pack-guide) - 
[Blockbench](https://blockbench.net)


# Credits

See [Credits.md](https://codeberg.org/clearKeep/crumble/src/branch/vanilla/CREDITS.md)
