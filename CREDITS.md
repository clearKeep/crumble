# Credits

This project contains work from:


[STYLOcraft](https://www.curseforge.com/minecraft/texture-packs/stylocraft) by Fishvap:
Acacia Trapdoor, Door, Planks & Log Top, Obsidian, Nether Bricks, Chiseled & Cracked Nether Bricks & Oak Door & Trapdoor.
License(s): 
CC-BY 3.0


Little Improvements: 
Inspiration for the lava bucket

- Sounds
	

[Hurt & Pain](https://freesound.org/people/thecheeseman/sounds/44429/) sounds by thecheeseman
License(s):
CC-BY 3.0	


[crossbow/loading_end](https://opengameart.org/content/2-metal-weapon-clicks) sounds by Michel Baradari (apollo-music.de)
License(s): 
CC BY 3.0


[elytra_loop](blenderfoundation.org) sounds (c) by The Blender Foundation
License(s):
CC BY 3.0


[Snowfall](https://opengameart.org/content/snowfall) by Joseph Gilbert / Kistol.
License(s): 
CC-BY 3.0


[Track 12 V2](https://opengameart.org/content/rizys-new-music-enjoy) by Rizy2
License(s):
CC0


[What We'll Build Next](https://git.minetest.land/MineClone5/MineClone5) by Dark Reaven/horizonchris96
License(s):
CC-BY-SA 3.0


[Splash](https://opengameart.org/content/water-splashes) (click_stereo) by Michael Baradari
License(s):
CC-BY 3.0


[Waiting Stillness](https://opengameart.org/content/waiting-stillness) (waiting_stillness) by Brylie Christopher Oxley
License(s):
CC-BY 4.0
